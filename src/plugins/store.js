import firebase from 'firebase/app'

export default {
    install(Vue) {
        Vue.prototype.$setStore = function (val) {
            localStorage.setItem('storeType', JSON.stringify(val))
        };
        Vue.prototype.$getStore = function () {
            return JSON.parse(localStorage.getItem('storeType'));
        };

        function getItem(key) {
            const storeType = Vue.prototype.$getStore();
            switch (storeType) {
                case 'local':
                    return JSON.parse(localStorage.getItem(key));
                case 'firebase':
                    return getFirebase();
                default:
                    return JSON.parse(localStorage.getItem(key));
            }
        }
        function setItem(key, val) {
            const storeType = Vue.prototype.$getStore();
            switch (storeType) {
                case 'local':
                    localStorage.setItem(key, JSON.stringify(val));
                    break;
                case 'firebase':
                    setFirebase(key, val);
                    break;
                default:
                    localStorage.setItem(key, JSON.stringify(val));
                    break;
            }
        }
        function getFirebase() {
            const starCountRef = firebase.database().ref('notes');
            starCountRef.on('value', function(snapshot) {
                console.log(snapshot);
            });
            return []
        }
        function setFirebase(key, val) {
            firebase.database().ref('notes').push(val)
        }

        Vue.prototype.$createNote = function (val) {
            const key = 'notes';
            if(!getItem(key)) {
                val.id = 0;
                val.comments = [];
                setItem(key, [val]);
            } else {
                const arr = getItem(key);
                val.id = getMaxId(arr) + 1;
                val.comments = [];
                arr.push(val);
                setItem(key, arr);
            }
        };
        Vue.prototype.$editNote = function (noteId, {name, content}) {
            const key = 'notes';
            if(getItem(key)) {
                const arr = getItem(key);
                arr.forEach(el => {
                    if(el.id === parseInt(noteId)) {
                        el.name = name;
                        el.content = content;
                    }
                });
                setItem(key, arr)
            }
        };
        Vue.prototype.$getNote = function (noteId) {
            const key = 'notes';
            if(getItem(key)) {
                const arr = getItem(key);
                return arr.filter(el => el.id === parseInt(noteId))[0];
            }
            return null;
        };
        Vue.prototype.$getNotes = function () {
            const key = 'notes';
            if(getItem(key)) {
                return getItem(key);
            }
            return [];
        };
        Vue.prototype.$delStore = function (noteId) {
            const key = 'notes';
            if(getItem(key)) {
                let arr = getItem(key);
                arr = arr.filter(el => el.id !== parseInt(noteId));
                setItem(key,arr)
            }
        };

        Vue.prototype.$saveComment = function (noteId, val) {
            const key = 'notes';
            const arr = getItem(key);
            arr.forEach(el => {
                if(el.id === parseInt(noteId)) {
                    val.id = getMaxId(el.comments) + 1;
                    val.date = new Date().toLocaleDateString();
                    el.comments.push(val)
                }
            });

            setItem(key, arr)
        };
        function getMaxId(arr) {
            return arr.reduce((prev, el) => el.id > prev ? el.id : prev, 0)
        }

    }
}