import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Settings from '../views/Settings.vue'
import CreateNote from '../views/CreateNote.vue'
import CreateComment from '../views/CreateComment.vue'
import EditNote from '../views/EditNote.vue'
import Note from '../views/Note.vue'

Vue.use(VueRouter);

  const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/settings',
    name: 'Settings',
    component: Settings
  },
  {
    path: '/createNote',
    name: 'CreateNote',
    component: CreateNote
  },
  {
    path: '/createComment/:id',
    name: 'CreateComment',
    component: CreateComment
  },
  {
    path: '/editNote/:id',
    name: 'EditNote',
    component: EditNote
  },
  {
    path: '/note/:id',
    name: 'Note',
    component: Note
  }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

export default router
