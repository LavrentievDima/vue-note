import Vue from 'vue'
import App from './App.vue'
import router from './router'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import firebase from "firebase/app";
import 'firebase/database'

import Store from '@/plugins/store'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.config.productionTip = false;

const firebaseConfig = {
    apiKey: "AIzaSyBaFgU6s59BqbMV8Nag7ScjA7RfguE_9O8",
    authDomain: "notes-d4234.firebaseapp.com",
    databaseURL: "https://notes-d4234.firebaseio.com",
    projectId: "notes-d4234",
    storageBucket: "notes-d4234.appspot.com",
    messagingSenderId: "749601787491",
    appId: "1:749601787491:web:a668233b453741bccdf256"
};
firebase.initializeApp(firebaseConfig);

Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.use(Store);

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
